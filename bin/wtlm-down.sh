#!/bin/bash
# HELP shutdown the system
for docker_id in $(docker container ps --filter label=wikitolearn-migration=1 -q)
do
  docker container stop $docker_id
done

for docker_id in $(docker container ps --filter label=wikitolearn-migration=1 -aq)
do
  docker container rm $docker_id
done
