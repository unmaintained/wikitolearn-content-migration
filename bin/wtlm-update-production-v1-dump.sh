#!/bin/bash
# HELP download the data from a backup NAS
cd `dirname "${BASH_SOURCE[0]}"`/..
if test ! -d dumps/
then
  mkdir dumps/
fi
if test ! -d dumps/production-v1-dump/
then
  mkdir dumps/production-v1-dump/
fi
. srv/wtl1-data-source.sh
if [[ "$DUMP_SOURCE" == "" ]]
then
  echo "You have to create \`srv/wtl1-data-source.sh\`"
  echo "With a line like \`DUMP_SOURCE=username@host:/path/\`"
  exit 1
fi

rsync \
    -i \
    -r \
    --delete \
    --delete-excluded \
    --exclude=/images/ \
    --exclude=/metawikitolearn* \
    --partial \
    --progress \
    --checksum \
    --no-owner \
    --no-group \
    --no-perms \
    --stats \
    $DUMP_SOURCE\
    dumps/production-v1-dump/
