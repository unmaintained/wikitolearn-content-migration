#!/bin/bash
# HELP export caches (parsoid and math-rendering example)
cd `dirname "${BASH_SOURCE[0]}"`/..

while ! docker exec -i wikitolearn-migration-destination mongo --quiet --eval '{ ping: 1 }'
do
  sleep 1
done

if test ! -d dumps/caches/
then
  mkdir dumps/caches/
fi

docker exec \
  -i wikitolearn-migration-destination \
  mongodump \
  --db migration_shared_data \
  --collection=parsoid_cache \
  --archive \
  --gzip > dumps/caches/parsoid-cache.dump.gz
du -hs dumps/caches/parsoid-cache.dump.gz

docker exec \
  -i wikitolearn-migration-destination \
  mongodump \
  --db migration_shared_data \
  --collection=formulas \
  --archive \
  --gzip > dumps/caches/formulas.dump.gz
du -hs dumps/caches/formulas.dump.gz
