#!/bin/bash
# HELP export destination mw pages collections
cd `dirname "${BASH_SOURCE[0]}"`/..

OUTDIR=dumps/
if test ! -d ${OUTDIR}
then
  mkdir ${OUTDIR}
fi
OUTDIR=$OUTDIR/output/
if test ! -d ${OUTDIR}
then
  mkdir ${OUTDIR}
fi

while [[ $(ls ${OUTDIR} | wc -l) -gt 5 ]]
do
  rm -Rfv ${OUTDIR}/$(ls ${OUTDIR} | sort -V  | head -1)
  sleep 1
done

OUTDIR=$OUTDIR/$(date +"%Y%m%d_%H%M%S")/
if test ! -d ${OUTDIR}
then
  mkdir ${OUTDIR}
fi

for SERVICE in courses chapters pages coursessecurity
do
  echo $SERVICE
  echo
  MONGO_OUTFILE=${OUTDIR}/$SERVICE.dump
  docker exec \
    -i wikitolearn-migration-destination \
    mongodump \
    --db $SERVICE \
    --archive > $MONGO_OUTFILE
done

docker container stop wikitolearn-migration-keycloak
sleep 1
MYSQL_OUTFILE=${OUTDIR}/keycloak.sql
docker container exec \
  -i wikitolearn-migration-keycloak-db \
  mysqldump -R -uroot -proot keycloak > $MYSQL_OUTFILE
docker container start wikitolearn-migration-keycloak

RESTORE_SCRIPT_NAME=${OUTDIR}/restore-dump.sh
cat <<EOF > $RESTORE_SCRIPT_NAME
#!/bin/bash
set -x

KEYCLOAK_CONTAINER_NAME=\$(docker container ls --filter label=com.docker.compose.service=keycloak -qa)
KEYCLOAK_DB_CONTAINER_NAME=\$(docker container ls --filter label=com.docker.compose.service=keycloak-db -qa)

docker container stop \$KEYCLOAK_CONTAINER_NAME
docker container exec -i \$KEYCLOAK_DB_CONTAINER_NAME mysql -uroot -proot -e "DROP DATABASE keycloak"
docker container exec -i \$KEYCLOAK_DB_CONTAINER_NAME mysql -uroot -proot -e "CREATE DATABASE keycloak"

docker container exec \
 	-i \$KEYCLOAK_DB_CONTAINER_NAME \
	mysql -uroot -proot keycloak < keycloak.sql

docker container start \$KEYCLOAK_CONTAINER_NAME

for t in chapters courses coursessecurity pages
do
	echo \${t}
  CONTAINER_NAME=\$(docker container ls --filter label=com.docker.compose.service=shared-mongodb-4 -qa)
	cat \${t}.dump | \
		docker exec -i \
		\$CONTAINER_NAME \
		mongorestore --drop --archive
done

EOF

chmod +x $RESTORE_SCRIPT_NAME

cd ${OUTDIR}
if test -f ../last_output.tar.gz
then
  rm -fv ../last_output.tar.gz
fi
tar -zcvf ../last_output.tar.gz *
