#!/bin/bash
# HELP run a little webui to test the data
cd `dirname "${BASH_SOURCE[0]}"`/..
OLD_PWD=`pwd`

. srv/ship-destinatione.sh
if [[ "$SHIP_HOST" == "" ]]
then
  echo "You have to create \`srv/ship-destinatione.sh\`"
  echo "With a line like \`SHIP_HOST=username@host\`"
  exit 1
fi

if ! ssh -t $SHIP_HOST screen -rd wikitolearn-content-migration
then
  rsync \
    -irz \
    --exclude=/.git \
    --exclude=/.gitignore \
    --delete \
    --delete-excluded \
    --partial \
    --progress \
    --checksum \
    --no-owner \
    --no-group \
    --stats \
    . \
    $SHIP_HOST:/srv/wikitolearn-content-migration/

  CMD=" . env-load.sh "
  if [[ "$1" == "--clean-all" ]]
  then
    CMD=$CMD" && wtlm-clean.sh "
    shift
  else
    CMD=$CMD" && wtlm-clean-destination.sh "
  fi
  CMD=$CMD" && wtlm-setup.sh "
  CMD=$CMD" && wtlm-run.sh "

  CMD=$CMD" && wtlm-output-export.sh "
  ssh -t $SHIP_HOST screen -mS wikitolearn-content-migration \
    "bash -c 'cd /srv/wikitolearn-content-migration/ && $CMD || sleep infinity'"
fi
