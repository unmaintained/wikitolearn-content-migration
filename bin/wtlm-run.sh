#!/bin/bash
# HELP run the pipeline
cd `dirname "${BASH_SOURCE[0]}"`/..
OLD_PWD=`pwd`

function pipeline_fix_names_and_permessions() {
  {
    echo 'C=5'
    ls  | grep '^[0-9][0-9][0-9]-' | while read file
    do
      echo 'OLD_FILE="'$file'"'
      echo 'SUFFIX="'$(echo ${file:4} | sed 's#_#-#g')'"'
      echo 'NEW_PREFIX=$(printf "%03d\n" $C)"-"'

      echo 'if [[ "$OLD_FILE" != "$NEW_PREFIX$SUFFIX" ]] ; then'
      echo '  mv "$OLD_FILE" "$NEW_PREFIX$SUFFIX"'
      echo 'fi'
      echo 'chmod +x "$NEW_PREFIX$SUFFIX"'

      echo 'C=$(($C+5))'
      echo
    done
  } | bash
}

cd "$OLD_PWD/src/scripts/"
pipeline_fix_names_and_permessions

for sub in "$OLD_PWD/src/scripts/"*
do
  if test -d $sub
  then
    cd $sub
    pipeline_fix_names_and_permessions
  else
    echo $sub" is not a directory"
    exit 1
  fi
done

# make README.md
cd `dirname "${BASH_SOURCE[0]}"`/..

{
  echo "# WikiToLearn Content Migration"
  echo
  echo "## bin/ content"
  {
    for file in bin/*
    do
      echo '* `'${file:4}'` '$(grep '^# HELP ' $file | cut -c 8-)
    done
  } | sort
  echo
  echo "## pipeline scripts"
  for dir in src/scripts/*
  do
    echo '* `'${dir:12}'`'
    for file in $dir/*
    do
      echo ' * `'${file##"$dir/"}'` '$(grep '^# HELP ' $file | cut -c 8-)
    done
  done
} > README.md

cd "$OLD_PWD/src/"

docker image build \
  -f Dockerfile \
  -t wikitolearn/migration-tool-pipeline \
  .

if [[ "$WTL_DOMAIN" == "" ]]
then
  export WTL_DOMAIN=wikitolearn.org
fi

HOST_IP=`ip addr show dev docker0 | grep "inet\b" | awk '{print $2}' | cut -d/ -f1`

echo HOST_IP=$HOST_IP

docker container run \
  --rm \
  -ti \
  -e WTL_DOMAIN=$WTL_DOMAIN \
  -e COURSES_BACKEND_URI=http://${HOST_IP}:10000 \
  -e CHAPTERS_BACKEND_URI=http://${HOST_IP}:10001 \
  -e PAGES_BACKEND_URI=http://${HOST_IP}:10002 \
  -e COURSESSECURITY_BACKEND_URI=http://${HOST_IP}:10003 \
  -e KEYCLOAK_URI=http://${HOST_IP}:9080 \
  --name wikitolearn-migration-runner-$$ \
  --hostname wikitolearn-migration-runner.lan \
  --user $(id -u) \
  --label wikitolearn-migration=1 \
  --tmpfs /tmp/ \
  -v $OLD_PWD/srv/:/srv/ \
  --link wikitolearn-migration-source:source \
  --link wikitolearn-migration-destination:destination \
  wikitolearn/migration-tool-pipeline $@
