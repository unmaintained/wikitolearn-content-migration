#!/bin/bash
# HELP deletes only destination databases
for docker_id in $(docker container ps --filter label=wikitolearn-migration-destination=1 -q)
do
  docker container kill $docker_id
done

for docker_id in $(docker container ps --filter label=wikitolearn-migration-destination=1 -aq)
do
  docker container rm $docker_id
done

for docker_volume_id in $(docker volume ls --filter label=wikitolearn-migration-destination=1 -q)
do
  docker volume rm $docker_volume_id
done
