
def __mysql_data_description(data,desc):
    if data == None:
        return None
    return_dict = {}
    for data_index in range(len(data)):
        v = data[data_index]
        if isinstance(v,bytes):
            v = v.decode('utf-8')
        return_dict[desc[data_index][0]] = v
    return return_dict

def mysql_fetchone(cursor):
    data = cursor.fetchone()
    desc = cursor.description
    return __mysql_data_description(data,desc)

def mysql_fetchall(cursor):
    return_list = []
    data_list = cursor.fetchall()
    desc = cursor.description
    for data in data_list:
        return_list.append(__mysql_data_description(data,desc))
    return return_list
