
def lookup_sub_page(mw_pages_collection, lang, sub_page_title, page, missing_pages):
    id_list = []
    mw_sub_page_title_candidates = []
    for page_title in page['all_used_titles']:
        mw_sub_page_title = page_title + "/" + sub_page_title
        mw_sub_page_title = mw_sub_page_title.replace(' ', '_')
        mw_sub_page_title = mw_sub_page_title.replace('__', '_')
        mw_sub_page_title_candidates.append(mw_sub_page_title)

    sub_page_objects = mw_pages_collection.find({
        "superlog.page_title": {"$in": mw_sub_page_title_candidates},
    })
    if sub_page_objects.count() > 0:
        for page_to_link in sub_page_objects:
            id_list.append(page_to_link['_id'])
    else:
        missing_dict = {'sub_page_title':sub_page_title, 'page_id':page['page_id']}
        if not missing_dict in missing_pages:
            missing_pages.append(missing_dict)
    return {
        'id_list':id_list,
        'sub_page_title':sub_page_title,
    }
