import os,os.path
import subprocess

def call_parsoid(lang, dns_domain, mw_text):
    parsoid_config_file = "/tmp/parsoid-{dns_domain}-config.yml".format(dns_domain=dns_domain)
    if not os.path.isfile(parsoid_config_file):
        with open(parsoid_config_file,"w") as fh:
            new_config = """
worker_heartbeat_timeout: 300000
logging:
 level: info
services:
- module: lib/index.js
  entrypoint: apiServiceWorker
  conf:
   mwApis:
   - uri: 'http://it.{dns_domain}/api.php'
     domain: 'it.{dns_domain}'
   - uri: 'http://en.{dns_domain}/api.php'
     domain: 'en.{dns_domain}'
   - uri: 'http://de.{dns_domain}/api.php'
     domain: 'de.{dns_domain}'
   - uri: 'http://es.{dns_domain}/api.php'
     domain: 'es.{dns_domain}'
   - uri: 'http://fr.{dns_domain}/api.php'
     domain: 'fr.{dns_domain}'
   - uri: 'http://pt.{dns_domain}/api.php'
     domain: 'pt.{dns_domain}'
   - uri: 'http://sv.{dns_domain}/api.php'
     domain: 'sv.{dns_domain}'
   - uri: 'http://ca.{dns_domain}/api.php'
     domain: 'ca.{dns_domain}'
   - uri: 'http://meta.{dns_domain}/api.php'
     domain: 'meta.{dns_domain}'
   - uri: 'http://pool.{dns_domain}/api.php'
     domain: 'pool.{dns_domain}'
   loadWMF: true
   useSelser: true
""".format(dns_domain=dns_domain)
            fh.write(new_config)

    p = subprocess.Popen([
        'parse.js',
        '--config',"/tmp/parsoid-{dns_domain}-config.yml".format(dns_domain=dns_domain),
        '--domain',lang + '.' + dns_domain,
    ],
    stdout=subprocess.PIPE,
    stdin=subprocess.PIPE,
    stderr=subprocess.PIPE)
    parse_dot_js_stdout,parse_dot_js_stderr = p.communicate(input=mw_text.encode('utf-8'))
    parsoid_output = parse_dot_js_stdout.decode()
    parsoid_error = parse_dot_js_stderr.decode()
    if 'node_modules/parsoid' in parsoid_error:
        raise Exception(parsoid_error)
    return parsoid_output
