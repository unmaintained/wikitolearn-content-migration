#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys ; sys.path.append('/opt/utils/')
from commons import *

source_shared_db = MySQLdb.connect('source', 'root', 'root', 'shared'+"wikitolearn", use_unicode=True, charset="utf8mb4")

sql = "SELECT "
sql += " user_id AS mw_user_id "
sql += " , "
sql += " user_name AS mw_user_name "
sql += " , "
sql += " user_email AS mw_user_email "
#sql += " , "
#sql += " user_password AS mw_user_password "
sql += " FROM user "
sql += " order by user_email, user_id desc"

cursor = source_db.cursor()
cursor.execute(sql)
mw_user_list = mysql_fetchall(cursor)

mw_users_collection = destination_db['users']
mw_users_collection.drop()
for user in tqdm(mw_user_list):
    search_for = {'$or': [
        {'mw_user_id': user['mw_user_id']},
        {'mw_user_email': user['mw_user_email'].lower()},
        {'mw_user_name': user['mw_user_name'].lower()},
    ]}
    mongo_object = mw_users_collection.find_one(search_for)

    new_entry = {}
    for key in user:
        if isinstance(user[key], int):
            new_entry[key] = [user[key]]
        elif len(user[key]) > 0:
            new_entry[key] = [user[key].lower()]

        if mongo_object != None:
            if key in mongo_object:
                if key in new_entry:
                    new_entry[key] += mongo_object[key]
                else:
                    new_entry[key] = mongo_object[key]

    for key in new_entry:
        new_entry[key] = sorted(list(set(new_entry[key])))

    if mongo_object == None:
        mw_users_collection.insert_one(new_entry)
    else:
        mw_users_collection.update_one(search_for,{'$set':new_entry})

for user in mw_users_collection.find():
    if len(user['mw_user_id']) > 1:
        pprint(user)
