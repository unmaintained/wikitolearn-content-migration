#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys ; sys.path.append('/opt/utils/')
from commons import *

mw_users_collection = destination_db['users']

all_users = mw_users_collection.find()
for user in tqdm(all_users,total=all_users.count()):
    sql = "SELECT  user_name AS mw_user_name,user_password AS mw_user_password FROM user "
    sql += " WHERE user_id = {} ".format(max(user['mw_user_id']))

    cursor = source_db.cursor()
    cursor.execute(sql)
    row_mw_user = mysql_fetchone(cursor)
    mw_user_name = row_mw_user['mw_user_name'].strip().lower()
    mw_user_password = row_mw_user['mw_user_password']

    keycloak_password_object = {}
    keycloak_password_object["type"] = "password"

    if mw_user_password[0:len(":pbkdf2:")] == ":pbkdf2:":
        password_hash_split = mw_user_password.split(':')
        keycloak_password_object["algorithm"] = "{}-{}".format(password_hash_split[1],password_hash_split[2])
        keycloak_password_object["hashIterations"] = int(password_hash_split[3])
        keycloak_password_object["salt"] = password_hash_split[5]
        keycloak_password_object["hashedSaltedValue"] = password_hash_split[6]
    elif mw_user_password[0:len(":B:")] == ":B:":
        keycloak_password_object["algorithm"] = "mediawiki-B-type"
        keycloak_password_object["hashedSaltedValue"] = mw_user_password
    else:
        logger.error("No password for {}".format(user))
        keycloak_password_object = None

    if keycloak_password_object != None:
        keycloak_user = {}
        keycloak_user['username'] = mw_user_name
        if 'mw_user_email' in user:
            keycloak_user['email'] = user['mw_user_email'][0]
        keycloak_user['credentials'] = [keycloak_password_object]
        keycloak_user["enabled"] = True
        keycloak_user["emailVerified"] = True
        keycloak_user["attributes"] = {
            "mw_id": max(user['mw_user_id']),
        }
        mw_id_alt_index = 1
        for alt_mw_id in user['mw_user_id']:
            if alt_mw_id != keycloak_user["attributes"]['mw_id']:
                keycloak_user["attributes"]['mw_alt_id_{}'.format(mw_id_alt_index)] = alt_mw_id
                mw_id_alt_index += 1

        mw_name_alt_index = 1
        for alt_mw_name in user['mw_user_name']:
            if alt_mw_name != mw_user_name:
                keycloak_user["attributes"]['mw_alt_name_{}'.format(mw_name_alt_index)] = alt_mw_name
                mw_name_alt_index += 1

        mw_users_collection.update_one({
            '_id':user['_id'],
        },{
            '$set':{'keycloak_user': keycloak_user},
        })
