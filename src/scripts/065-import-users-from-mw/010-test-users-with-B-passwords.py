#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys ; sys.path.append('/opt/utils/')
from commons import *

all_uesrs_id = []

# search for every editor of a content page
for lang in ['it', 'en', 'de', 'es', 'fr', 'pt', 'sv', 'ca']:
    mw_pages_collection = destination_link[lang]['mw_pages']

    all_pages = mw_pages_collection.find({
        'category': "CourseLevelThree",
    })

    for page in tqdm(all_pages, total=all_pages.count(), desc=lang):
        for superlog_entry in page['superlog']:
            all_uesrs_id += [str(superlog_entry['user_id'])]
            if 'user_ids' in superlog_entry:
                for user_id in superlog_entry['user_ids']:
                    all_uesrs_id += [str(user_id)]

source_shared_db = MySQLdb.connect('source', 'root', 'root', 'shared'+"wikitolearn", use_unicode=True, charset="utf8mb4")

if len(all_uesrs_id) > 0:
    sql = "SELECT "
    sql += " user_id "
    sql += " , "
    sql += " user_name "
    sql += " , "
    sql += " user_real_name "
    sql += " FROM `user` WHERE user_id IN ("
    sql += ", ".join(sorted(list(set(all_uesrs_id))))
    sql += ") AND user_password LIKE ':B:%' AND user_email = ''"

    cursor = source_db.cursor()
    cursor.execute(sql)
    user_list = mysql_fetchall(cursor)

    problematic_user_ids = []
    for user in user_list:
        problematic_user_ids.append(user['user_id'])

    logger.debug("Users with B password and without email: {}".format(problematic_user_ids))

    for lang in ['it', 'en', 'de', 'es', 'fr', 'pt', 'sv', 'ca']:
        mw_pages_collection = destination_link[lang]['mw_pages']

        pages_with_issues = mw_pages_collection.find({
            'category': "CourseLevelThree",
            'superlog.user_ids': {'$in': problematic_user_ids}
        })

        for page in pages_with_issues:
            logger.debug("Page for lang={}: {}".format(lang, requests.utils.quote(page['page_title'])))
