#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys ; sys.path.append('/opt/utils/')
from commons import *

mw_pages_collection = destination_db['mw_pages']

all_pages = mw_pages_collection.find()

for page in tqdm(all_pages, total=all_pages.count()):
    for superlog_index in tqdm(range(len(page['superlog']))):
        page['superlog'][superlog_index]['page_html'] = parse_mw_text(page['superlog'][superlog_index]['page_wikitext'])
    mw_pages_collection.update_one({'_id':page['_id']}, {'$set':{
        'superlog':page['superlog'],
    }})
