#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys ; sys.path.append('/opt/utils/')
from commons import *

mw_pages_collection = destination_db['mw_pages']

# no need for redirects
db_filter_1 = {
    'superlog.page_html':re.compile(".*" + re.escape('rel="mw:PageProp/redirect"') + ".*")
}

# no need for redirects
db_filter_2 = {'$and':[
    {'superlog.is_redirect':1},
    {'superlog.is_redirect':{'$ne':0}},
]}

db_filter = {'$or':[db_filter_1, db_filter_2]}

redirect_ids = []

print("Doing a big query (regex involved)")
search_result = mw_pages_collection.find(db_filter) #.limit(1)
for redirect_obejct in tqdm(search_result, total=search_result.count()):
    redirect_ids.append(redirect_obejct['_id'])

set_redirect_ids = set(redirect_ids)

pages = mw_pages_collection.find({'superlog.sub_pages.id_list':{"$in": redirect_ids}})
print("Found {} pages".format(pages.count()))
for page in tqdm(pages, total=pages.count()):
    for eventlog_entry in page['superlog']:
        for sub_page in eventlog_entry['sub_pages']:
            for redirect_id in redirect_ids:
                if redirect_id in sub_page['id_list']:
                    sub_page['id_list'].remove(redirect_id)
    mw_pages_collection.update_one({'_id':page['_id']}, {'$set':{
        'superlog':page['superlog'],
    }})

print("Delete redirects")
mw_pages_collection.delete_many({'_id':{'$in': redirect_ids}})
