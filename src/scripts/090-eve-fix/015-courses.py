#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys ; sys.path.append('/opt/utils/')
from commons import *

courses_collection = destination_link['courses']['courses']
courses_versions_collection = destination_link['courses']['courses_versions']
courses_base_uri = os.environ.get("COURSES_BACKEND_URI") + "/v1/courses/"

courses_collection.update_many({}, {'$unset': {'_etag':1}})
courses_versions_collection.update_many({}, {'$unset': {'_etag':1}})

courses_find = courses_collection.find()

for page in tqdm(courses_find, total=courses_find.count()):
    page_versions_find = courses_versions_collection.find({'_id_document':page['_id']})

    language = page['language']

    # temp collection
    wtl_revisions_collection = destination_link[language]['wtl_revisions']
    # temp page
    wtl_page = wtl_revisions_collection.find_one({
        'eve.id': str(page['_id']),
        'eve.version': 1,
    })

    # HACK update eve page
    courses_collection.update_one({
        '_id': page['_id'],
    }, {'$set': {
        '_created': wtl_page['timestamp'],
        '_updated': wtl_page['timestamp'],
    }})

    for page_version in tqdm(page_versions_find, total=page_versions_find.count()):
        wtl_page_version = wtl_revisions_collection.find_one({
            'eve.id': str(page_version['_id_document']),
            'eve.version': page_version['_version'],
        })
        courses_versions_collection.update_one({
            '_id_document': page_version['_id_document'],
            '_version': page_version['_version'],
        }, {'$set': {
            '_updated': wtl_page_version['timestamp'],
        }})

        # update eve page last update timestamp
        courses_collection.update_one({
            '_id': page['_id'],
            '_updated': {'$lt': wtl_page_version['timestamp']},
        }, {'$set': {
            '_updated': wtl_page_version['timestamp'],
        }})


    # HACK to fix etag
    page_versions_find = courses_versions_collection.find({'_id_document':page['_id']})

    _etag = requests.get(courses_base_uri + str(page['_id'])).json()['_etag']
    courses_collection.update_one({
        '_id': page['_id'],
    }, {'$set': {
        '_etag': _etag,
    }})
    for page_version in page_versions_find:
        _version_etag = requests.get(courses_base_uri + str(page['_id'])  + "?version=" + str(page_version['_version']) ).json()['_etag']
        courses_versions_collection.update_one({
            '_id': page_version['_id'],
        }, {'$set': {
            '_etag': _version_etag,
        }})
