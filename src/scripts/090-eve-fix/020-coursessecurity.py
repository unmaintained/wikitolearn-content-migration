#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys ; sys.path.append('/opt/utils/')
from commons import *

courses_collection = destination_link['courses']['courses']
courses_versions_collection = destination_link['courses']['courses_versions']

coursessecurity_collection = destination_link['coursessecurity']['coursessecurity']
coursessecurity_versions_collection = destination_link['coursessecurity']['coursessecurity_versions']
coursessecurity_base_uri = os.environ.get("COURSESSECURITY_BACKEND_URI") + "/v1/coursessecurity/"

coursessecurity_collection.update_many({}, {'$unset': {'_etag':1}})
coursessecurity_versions_collection.update_many({}, {'$unset': {'_etag':1}})

coursessecurity_find = coursessecurity_collection.find()

for page in tqdm(coursessecurity_find, total=coursessecurity_find.count()):
    page_versions_find = coursessecurity_versions_collection.find({'_id_document':page['_id']})

    page_created_timestamp = courses_collection.find_one({
        '_id': page['course']['_id'],
    })['_created']
    page_update_timestamp = courses_versions_collection.find_one({
        '_id_document': page['course']['_id'],
        '_version': page['course']['_version'],
    })['_updated']

    # HACK update eve page
    coursessecurity_collection.update_one({
        '_id': page['_id'],
    }, {'$set': {
        '_created': page_created_timestamp,
        '_updated': page_update_timestamp,
    }})

    for page_version in tqdm(page_versions_find, total=page_versions_find.count()):
        page_version_update_timestamp = courses_versions_collection.find_one({
            '_id_document': page_version['course']['_id'],
            '_version': page_version['course']['_version'],
        })['_updated']
        coursessecurity_versions_collection.update_one({
            '_id_document': page_version['_id_document'],
            '_version': page_version['_version'],
        }, {'$set': {
            '_updated': page_version_update_timestamp,
        }})

    # HACK to fix etag
    page_versions_find = coursessecurity_versions_collection.find({'_id_document':page['_id']})

    _etag = requests.get(coursessecurity_base_uri + str(page['_id'])).json()['_etag']
    coursessecurity_collection.update_one({
        '_id': page['_id'],
    }, {'$set': {
        '_etag': _etag,
    }})
    for page_version in page_versions_find:
        _version_etag = requests.get(coursessecurity_base_uri + str(page['_id'])  + "?version=" + str(page_version['_version']) ).json()['_etag']
        coursessecurity_versions_collection.update_one({
            '_id': page_version['_id'],
        }, {'$set': {
            '_etag': _version_etag,
        }})
