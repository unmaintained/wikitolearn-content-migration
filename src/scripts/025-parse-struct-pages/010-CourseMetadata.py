#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys ; sys.path.append('/opt/utils/')
from commons import *

mw_pages_collection = destination_db['mw_pages']

db_filter = {'category':"CourseRoot"}
search_result = mw_pages_collection.find(db_filter)

for page in tqdm(search_result, total=search_result.count()):
    mw_pages_collection.update_many({
        'superlog.page_title': {'$in': page['all_used_titles']},
        'page_namespace': 2900
    }, {'$set': {'category':"CourseMetadata"}})

mw_pages_collection.update_many({'category':"CourseMetadata"}, {'$set': {'page_in_use':True}})

metadata_known_labels = [
    'bibliography',
    'reviewedon',
    'topic',
    'isimported',
    'description',
    'externalreferences',
    'books',
    'hasbadge',
    'originalauthors',
    'isreviewed',
    'exercises',
]
metadata_labels = []

db_filter = {'category':"CourseMetadata"}
search_result = mw_pages_collection.find(db_filter)
for page in tqdm(search_result, total=search_result.count()):

    for superlog_entry in page['superlog']:
        superlog_entry['metadata'] = {}
        regex = r"\<section begin\=(?P<begin>[^\s]*) \/\>(?P<payload>.*)\<section end=(?P<end>[^\s]*) \/\>"
        matches = re.finditer(regex, superlog_entry['page_wikitext'])
        for match in matches:
            m_begin = match.groupdict().get('begin')
            m_payload = match.groupdict().get('payload')
            m_end = match.groupdict().get('end')
            if m_begin != m_end:
                raise ValueError("{} != {}".format(m_begin, m_end))
            superlog_entry['metadata'][m_begin] = m_payload
            metadata_labels.append(m_begin)
    mw_pages_collection.update_one({
        '_id':page['_id'],
    },{'$set':{
        'superlog':page['superlog']
    }})

unknown_labels = list(set(metadata_labels) - set(metadata_known_labels))
if len(unknown_labels) > 0:
    print(unknown_labels)
    sys.exit(1)
