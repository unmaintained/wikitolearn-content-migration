#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys ; sys.path.append('/opt/utils/')
from commons import *

mw_pages_collection = destination_db['mw_pages']

db_filter = {
    'page_in_use':{'$exists': False},
}
search_obj = mw_pages_collection.find(db_filter)
print("Delete {} documents {}".format(
    search_obj.count(),
    db_filter
))
mw_pages_collection.delete_many(db_filter)


mw_pages_collection.update_many({}, {'$unset': {
    'page_in_use':1}
})
