#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys ; sys.path.append('/opt/utils/')
from commons import *

mw_pages_collection = destination_db['mw_pages']

lv3_missing_pages = []

all_lv3_ids = mw_pages_collection.find({
    'category':"CourseLevelTwo"
}).distinct("superlog.sub_pages.id_list")

for lv3_id in tqdm(all_lv3_ids):
    mw_pages_collection.update_one({'_id':lv3_id},{'$set':{
        'category':'CourseLevelThree',
        'page_in_use':True,
    }})
