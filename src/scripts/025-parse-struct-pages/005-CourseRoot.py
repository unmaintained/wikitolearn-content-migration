#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys ; sys.path.append('/opt/utils/')
from commons import *

mw_pages_collection = destination_db['mw_pages']

db_filter = {'category':"CourseRoot"}
search_result = mw_pages_collection.find(db_filter)

lv2_missing_pages = []

for page in tqdm(search_result, total=search_result.count()):
    new_superlog = []
    for superlog_entry in tqdm(page['superlog']):
        root_text = superlog_entry['page_wikitext']

        regex = r"\{\{CourseLevelTwo\|(?P<title>.*)\}\}"
        matches = re.finditer(regex, root_text)

        sub_pages = []
        sub_pages_titles = []
        for match in matches:
            page_title = match.groupdict()['title'].strip()

            sub_pages_titles.append(page_title)
            sub_pages.append(lookup_sub_page(mw_pages_collection, lang, page_title, page, lv2_missing_pages))

        superlog_entry['sub_pages_titles'] = sub_pages_titles
        superlog_entry['sub_pages'] = sub_pages
        new_superlog.append(superlog_entry)
    mw_pages_collection.update_many({'_id':page['_id']},{'$set':{'superlog':new_superlog}})

mw_pages_collection.update_many(db_filter, {'$set': {'page_in_use':True}})

asset_filename = "/opt/assets/" + lang + "/lv2_missing_pages.yml"
if os.path.isfile(asset_filename):
    with open(asset_filename) as fh:
        lv2_missing_from_file = yaml.load(fh)
        if lv2_missing_from_file != None:
            for l in lv2_missing_from_file:
                if l in lv2_missing_pages:
                    lv2_missing_pages.remove(l)

if len(lv2_missing_pages) > 0:
    print("\n\nError!")
    lv2_missing_pages = sorted(lv2_missing_pages, key=lambda k: k['sub_page_title'])
    print(yaml.dump(lv2_missing_pages, default_flow_style=False))
    sys.exit(1)
