#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys ; sys.path.append('/opt/utils/')
from commons import *

wtl_revisions_collection = destination_db['wtl_revisions']

all_subpagess = wtl_revisions_collection.find().distinct("mw_page_id")

for page_id in tqdm(all_subpagess):
    min_timestamp = None
    next_object = None

    prev_deleted = None
    prev_payload = None
    prev_page_title = None
    prev_id = None
    prev_user_ids = None

    considered_ids = []

    while min_timestamp == None or next_object != None:
        filter_obj = {'mw_page_id': page_id}
        if min_timestamp != None:
            filter_obj['timestamp'] = {'$gte': min_timestamp}

        filter_obj['_id'] = {'$nin': considered_ids}

        next_object = wtl_revisions_collection.find_one(filter_obj, sort=[("revision_number", pymongo.ASCENDING)])
        if next_object != None:
            new_deleted = next_object['deleted']
            new_payload = str(next_object['payload'])
            new_page_title = next_object['page_title']
            new_id = next_object['_id']
            new_user_ids = next_object['user_ids']
            considered_ids.append(new_id)

            if  min_timestamp != None and prev_deleted == new_deleted and prev_payload == new_payload and prev_page_title == new_page_title:
                wtl_revisions_collection.update_one({'_id':new_id},{'$set':
                    {'not_required': True},
                })

                prev_user_ids = sorted(list(set(prev_user_ids + new_user_ids)))
                wtl_revisions_collection.update_one({'_id':prev_id},{'$set':
                    {'user_ids': prev_user_ids},
                })
            else:
                prev_deleted = new_deleted
                prev_payload = new_payload
                prev_page_title = new_page_title
                prev_id = new_id
                prev_user_ids = new_user_ids

            min_timestamp = next_object['timestamp']


db_filter = {
    'not_required': True,
}
search_obj = wtl_revisions_collection.find(db_filter)
print("Delete {} documents {}".format(
    search_obj.count(),
    db_filter
))
wtl_revisions_collection.delete_many(db_filter)

# wtl_revisions_collection.update_many({}, {'$unset': {
#     'not_required':1}
# })
