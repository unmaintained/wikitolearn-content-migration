#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys ; sys.path.append('/opt/utils/')
from commons import *

wtl_revisions_collection = destination_db['wtl_revisions']

course_root_revisions = wtl_revisions_collection.find({
    'category': 'CourseRoot',
})
for course_root_revision in tqdm(course_root_revisions, total=course_root_revisions.count()):
    new_course_title = course_root_revision['page_title']
    new_course_title = new_course_title.replace('_', ' ')
    course_owner = None

    if 'mw_username_course_owner' in course_root_revision['payload']:
        course_owner = course_root_revision['mw_username_course_owner']['payload']

    if '/' in new_course_title:
        course_owner = new_course_title[0:new_course_title.find('/')]
        new_course_title = new_course_title[len(course_owner)+1:]

    wtl_revisions_collection.update_one({
        '_id':course_root_revision['_id'],
    }, {'$set': {
        'page_title': new_course_title,
        'payload.mw_username_course_owner': course_owner,
    }})
