#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys ; sys.path.append('/opt/utils/')
from commons import *

wtl_revisions_collection = destination_db['wtl_revisions']

for cat in ['CourseLevelThree', 'CourseLevelTwo']:
    print(cat)
    lv3_revisions = wtl_revisions_collection.find({
        'category': cat,
    })
    for lv3_revision in tqdm(lv3_revisions, total=lv3_revisions.count()):

        parents_all_names = all_sub_page_id = wtl_revisions_collection.find({
            'payload.subpages.id':lv3_revision['mw_page_id'],
        }).distinct("page_title")

        new_title = lv3_revision['page_title']

        if '/' in new_title:
            parent_name = None
            for parent_name_candidate in parents_all_names:
                if new_title[0:len(parent_name_candidate) + 1] == parent_name_candidate + "/":
                    parent_name = parent_name_candidate

            if parent_name != None:
                new_title = new_title[len(parent_name_candidate) + 1:]
                new_title = new_title.replace('_', ' ')

                if not '/' in new_title:
                    wtl_revisions_collection.update_one({
                        '_id':lv3_revision['_id'],
                    }, {'$set': {
                        'page_title': new_title,
                    }})
