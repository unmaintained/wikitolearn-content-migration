#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys ; sys.path.append('/opt/utils/')
from commons import *

mw_pages_collection = destination_db['mw_pages']

all_pages = mw_pages_collection.find()

for page in tqdm(all_pages, total=all_pages.count()):
    for superevent_item in page['superlog']:
        if superevent_item['rev_text_id'] != None:
            sql = ""
            sql += "SELECT old_text AS `page_wikitext` FROM `text` WHERE `text`.old_id = {}".format(superevent_item['rev_text_id'])

            cursor = source_db.cursor()
            cursor.execute(sql)
            text_row = mysql_fetchall(cursor)

            if len(text_row) == 1:
                superevent_item['page_wikitext'] = text_row[0]['page_wikitext']
            else:
                sys.exit(1)

    mw_pages_collection.update_one({'_id':page['_id']}, {'$set':page})
