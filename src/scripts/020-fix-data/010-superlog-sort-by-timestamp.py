#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys ; sys.path.append('/opt/utils/')
from commons import *


mw_pages_collection = destination_db['mw_pages']
mw_pages_collection.aggregate([
    {'$unwind': "$superlog"},
    {'$sort': {"superlog.timestamp":1}},
    {'$group': {'_id':"$_id", 'superlog': {'$push':"$superlog"}}}
],  allowDiskUse=True);
