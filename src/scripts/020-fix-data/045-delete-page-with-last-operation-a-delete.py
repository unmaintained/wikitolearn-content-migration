#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys ; sys.path.append('/opt/utils/')
from commons import *

mw_pages_collection = destination_db['mw_pages']
mw_pages_collection.update_many({}, {'$unset': {
    'to_delete': True,
}})

all_pages = mw_pages_collection.find()
for page in tqdm(all_pages, total=all_pages.count()):
    if page['superlog'][len(page['superlog'])-1]['action'] == "delete" and \
            page['superlog'][len(page['superlog'])-1]['type'] == "delete":
        pages_with_same_title = mw_pages_collection.find({
            'all_used_titles': page['page_title'],
        })
        if pages_with_same_title.count() > 1:
            mw_pages_collection.update_one({'_id':page['_id']}, {'$set':{
                'to_delete':True
            }})

pages_to_delete = mw_pages_collection.find({
    'to_delete':{'$exists':True}
})

print("To delete {} pages".format(pages_to_delete.count()))

mw_pages_collection.delete_many({
    'to_delete':{'$exists':True}
})
