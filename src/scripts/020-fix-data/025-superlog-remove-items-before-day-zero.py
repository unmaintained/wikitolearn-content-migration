#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys ; sys.path.append('/opt/utils/')
from commons import *

mw_pages_collection = destination_db['mw_pages']

all_pages = mw_pages_collection.find()

for page in tqdm(all_pages, total=all_pages.count()):
    last_content_before_day_zero_index = None
    for superlog_index in range(len(page['superlog'])):
        if page['superlog'][superlog_index]['timestamp'] < day_zero and \
             page['superlog'][superlog_index]['type'] == None:
            last_content_before_day_zero_index = superlog_index
    if last_content_before_day_zero_index != None:
        removed_user_ids = []
        for superlog_index in range(last_content_before_day_zero_index):
            if page['superlog'][superlog_index]['type'] == None:
                removed_user_ids += [page['superlog'][superlog_index]['user_id']]

        new_superlog = []
        for superlog_index in range(last_content_before_day_zero_index, len(page['superlog'])):
            new_superlog.append(page['superlog'][superlog_index])

        new_superlog[0]['user_ids'] = list(sorted(set(removed_user_ids)))

        mw_pages_collection.update_one({'_id':page['_id']}, {'$set':{
            'superlog':new_superlog,
        }})
