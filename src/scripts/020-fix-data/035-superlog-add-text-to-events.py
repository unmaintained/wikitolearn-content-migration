#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys ; sys.path.append('/opt/utils/')
from commons import *

mw_pages_collection = destination_db['mw_pages']

all_pages = mw_pages_collection.find()

for page in tqdm(all_pages, total=all_pages.count()):
    superlog = page['superlog']

    current_page_wikitext = None
    current_rev_text_id = None
    for action_index in range(len(superlog)):
        if superlog[action_index]['type'] != None:
            if current_page_wikitext == None:
                raise ValueError("First event must have a text")
            superlog[action_index]['page_wikitext'] = current_page_wikitext
            superlog[action_index]['rev_text_id'] = current_rev_text_id
        else:
            if 'page_wikitext' in superlog[action_index]:
                current_page_wikitext = superlog[action_index]['page_wikitext']
                current_rev_text_id = superlog[action_index]['rev_text_id']
            else:
                superlog[action_index]['page_wikitext'] = current_page_wikitext
                superlog[action_index]['rev_text_id'] = current_rev_text_id

    page['superlog'] = superlog
    mw_pages_collection.update_one({'_id':page['_id']}, {'$set':page})
