#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys ; sys.path.append('/opt/utils/')
from commons import *

mw_pages_collection = destination_db['mw_pages']

mw_pages_collection.update_many({}, {'$set':{
    'title_to_fix':False
}})

all_pages = mw_pages_collection.find()

for page in tqdm(all_pages, total=all_pages.count()):
    superlog = page['superlog']
    current_title = page['page_title']
    current_ns = page['page_namespace']

    all_used_titles = [current_title]

    for action_index in range(len(superlog)-1, -1, -1):
        if 'params_move' in superlog[action_index]:
            params_target_title = superlog[action_index]['params_move']['4::target'].replace(' ', '_')
            if ':' in params_target_title:
                params_target_title = params_target_title[params_target_title.index(':')+1:]
            if params_target_title != current_title:
                # HACK
                if params_target_title not in all_used_titles and \
                    current_title not in all_used_titles:
                    # HACK
                    print('Impossible move: ' + params_target_title + ' != ' + current_title)
                    print("\n")
                    pprint( superlog )
                    print("\n\n")
                    raise ValueError()
                    mw_pages_collection.update_one({'_id':page['_id']}, {'$set':{
                        'title_to_fix':True
                    }})
                # else:
                #     print("Duplicated move from/to a previous page_title")
            prev_title = current_title
            prev_ns = current_ns

            current_title = superlog[action_index]['page_title']

            current_ns = superlog[action_index]['page_namespace']

            superlog[action_index]['page_title'] = prev_title
            superlog[action_index]['page_namespace'] = prev_ns

            superlog[action_index]['previous_page_title'] = current_title
            superlog[action_index]['previous_page_namespace'] = current_ns

        else:
            superlog[action_index]['page_title'] = current_title
            superlog[action_index]['page_namespace'] = current_ns
        if current_title not in all_used_titles:
            all_used_titles.append(current_title)

    page['superlog'] = superlog
    page['all_used_titles'] = all_used_titles
    mw_pages_collection.update_one({'_id':page['_id']}, {'$set':page})


pages_to_be_fixed  = mw_pages_collection.find({
    'title_to_fix':True
}).sort('page_title', pymongo.ASCENDING)
# pages_to_be_fixed_list = []
# for page in pages_to_be_fixed:
#     pages_to_be_fixed_list.append(page['page_title'])
# if len(pages_to_be_fixed_list) > 0:
#     print("error!")
#     print(yaml.dump(pages_to_be_fixed_list, default_flow_style=False))
#     sys.exit(1)

mw_pages_collection.update_many({}, {'$unset': {
    'title_to_fix':1}
})
