#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys ; sys.path.append('/opt/utils/')
from commons import *

mw_pages_collection = destination_db['mw_pages']

all_pages = mw_pages_collection.find()

for page in tqdm(all_pages, total=all_pages.count()):
    deleted = False
    superlog = page['superlog']

    for superlog_entry in superlog:
        if superlog_entry['type'] == "delete":
            if superlog_entry['action'] == "delete":
                deleted = True
            elif superlog_entry['action'] == "restore":
                deleted = False
        superlog_entry['deleted'] = deleted

    mw_pages_collection.update_one({'_id':page['_id']}, {'$set':{
        'superlog': superlog,
    }})
