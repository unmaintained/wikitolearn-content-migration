#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys ; sys.path.append('/opt/utils/')
from commons import *

mw_pages_collection = destination_db['mw_pages']

all_root_pages = mw_pages_collection.find()
for root_page in tqdm(all_root_pages, total=all_root_pages.count()):
    root_page['superlog'][-1]['timestamp_next'] = root_page['timestamp_next']
    for action_index in range(1,len(root_page['superlog'])):
        root_page['superlog'][action_index-1]['timestamp_next'] = root_page['superlog'][action_index]['timestamp']
    mw_pages_collection.update_one({'_id':root_page['_id']}, {'$set':root_page})
