#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys ; sys.path.append('/opt/utils/')
from commons import *

def keys_to_string(in_dict):
    out_dict = {}
    for key in in_dict:
        if isinstance(in_dict[key], dict):
            out_dict[str(key)] = keys_to_string(in_dict[key])
        else:
            out_dict[str(key)] = in_dict[key]
    return out_dict

mw_pages_collection = destination_db['mw_pages']

all_pages = mw_pages_collection.find()

for page in tqdm(all_pages, total=all_pages.count()):
    if len(page['superlog']) > 0:
        # HACK phpserialize for log_event params column
        for log_event in page['superlog']:
            if log_event['action'] == None and log_event['type'] == None: # mediawiki actual revision
                pass
            elif log_event['type'] == "move" and (log_event['action'] == "move" or log_event['action'] == "move_redir"):
                if not 'params_move' in log_event:
                    if log_event['params'][0:len("File:")] == "File:":
                        pass # FIXME log_event['params_move'] = {'4::target': log_event['params'].strip()}
                    elif log_event['params'][0:len("Dispensa:")] == "Dispensa:":
                        pass # FIXME log_event['params_move'] = {'4::target': log_event['params'].strip()}
                    else:
                        new_params = phpserialize.loads(log_event['params'].encode('utf-8'))
                        for key in new_params.keys():
                            current_key = key
                            if isinstance(key,bytes):
                                current_key = key.decode('utf-8')
                                new_params[current_key] = new_params[key]
                                del new_params[key]
                            if isinstance(new_params[current_key], bytes):
                                new_params[current_key] = new_params[current_key].decode('utf-8')
                        log_event['params_move'] = keys_to_string(new_params)
            elif log_event['type'] == "delete" and log_event['action'] == "delete":
                if log_event['params'] != "a:0:{}" and log_event['params'] != "":
                    print("DELETE")
                    print(log_event)
                    raise Exception("Invalid params")
            elif log_event['type'] == "delete" and log_event['action'] == "restore":
                if log_event['params'] != "a:0:{}" and log_event['params'] != "":
                    print("RESTORE")
                    print(log_event)
                    raise Exception("Invalid params")
            elif log_event['type'] == 'delete' and log_event['action'] == "delete_redir":
                pass
            else:
                pprint(log_event)
                print("\n\n\n")
                raise Exception("exc")

        mw_pages_collection.update_one({'_id':page['_id']}, {'$set':page})
