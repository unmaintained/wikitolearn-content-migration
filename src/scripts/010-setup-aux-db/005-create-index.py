#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys ; sys.path.append('/opt/utils/')
from commons import *

for label in [
        "formula_hash",
        "formula_type",
    ]:
    try:
        destination_aux_db['formulas'].create_index(label)
    except Exception as exc:
        print(label)
        print(exc)
        print("\n\n")

for label in [
        "mw_hash",
        "tested_hostname",
        "last_update",
    ]:
    try:
        destination_aux_db['parsoid_cache'].create_index(label)
    except Exception as exc:
        print(label)
        print(exc)
        print("\n\n")
