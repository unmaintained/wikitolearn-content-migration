#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys ; sys.path.append('/opt/utils/')
from commons import *

wtl_revisions_collection = destination_db['wtl_revisions']

for cat in ["CourseLevelThree", "CourseLevelTwo"]:
    all_sub_page_id = wtl_revisions_collection.find({
        'category':cat,
    }).distinct("mw_page_id")

    for sub_page_id in tqdm(all_sub_page_id):
        structural_subpages = wtl_revisions_collection.find({
            'payload.subpages.id':sub_page_id,
        }).distinct("mw_page_id")
        for structural_page_id in structural_subpages:
            sub_revisions = list(wtl_revisions_collection.find({
                'mw_page_id':sub_page_id,
            }).sort("revision_number",pymongo.ASCENDING))
            structural_revisions = list(wtl_revisions_collection.find({
                'mw_page_id':structural_page_id,
            }).sort("revision_number",pymongo.ASCENDING))

            current_sub_page = sub_revisions.pop(0)
            current_structural_page = structural_revisions.pop(0)
            new_structural_revisions_list = []
            run = True
            while run:
                # FIXME
                # the structural page can skip the reference to the sub page
                # in this case maybe a revision is lost?
                for sub_page in current_structural_page['payload']['subpages']:
                    if sub_page['id'] == sub_page_id:
                        sub_page['rev'] = current_sub_page['revision_number']

                new_structural_revisions_list.append(copy.deepcopy(current_structural_page))

                run = (len(sub_revisions) > 0 or len(structural_revisions) > 0)
                if run:
                    if len(sub_revisions) > 0 and len(structural_revisions) > 0:
                        if sub_revisions[0]['timestamp'] < structural_revisions[0]['timestamp']:
                            current_sub_page = sub_revisions.pop(0)
                            current_structural_page['timestamp'] = current_sub_page['timestamp']
                            if '_id' in current_structural_page:
                                del current_structural_page['_id']
                        elif sub_revisions[0]['timestamp'] >= structural_revisions[0]['timestamp']:
                            current_structural_page = structural_revisions.pop(0)
                        else:
                            sys.exit(1)
                    elif len(sub_revisions) > 0 and len(structural_revisions) <= 0:
                        current_sub_page = sub_revisions.pop(0)
                        current_structural_page['timestamp'] = current_sub_page['timestamp']
                        if '_id' in current_structural_page:
                            del current_structural_page['_id']
                    elif len(sub_revisions) <= 0 and len(structural_revisions) > 0:
                        current_structural_page = structural_revisions.pop(0)

            wtl_revisions_collection.delete_many({
                'mw_page_id':structural_page_id,
            })
            new_structural_revision_revision_number = 0
            for new_structural_revision in new_structural_revisions_list:
                new_structural_revision['revision_number'] = new_structural_revision_revision_number
                new_structural_revision_revision_number += 1
                wtl_revisions_collection.insert_one(new_structural_revision)
