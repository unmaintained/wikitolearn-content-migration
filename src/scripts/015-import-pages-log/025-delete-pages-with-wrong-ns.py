#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys ; sys.path.append('/opt/utils/')
from commons import *

mw_pages_collection = destination_db['mw_pages']
all_pages = mw_pages_collection.find()

namespaces_list = {
    'namespace_user': 2,
    'namespace_course': 2800,
    'namespace_course_metadata': 2900,
}
for page in tqdm(all_pages, total=all_pages.count()):
    if page['page_namespace'] not in namespaces_list.values():
        if len(page['superlog']) > 0:
            if page['superlog'][len(page['superlog'])-1]['page_namespace'] not in namespaces_list.values():
                mw_pages_collection.update_one({'_id':page['_id']}, {'$set':{
                    'to_delete':True
                }})
        else:
            mw_pages_collection.update_one({'_id':page['_id']}, {'$set':{
                'to_delete':True
            }})

pages_to_delete = mw_pages_collection.find({
    'to_delete':{'$exists':True}
})

print("To delete {} pages".format(pages_to_delete.count()))

mw_pages_collection.delete_many({
    'to_delete':{'$exists':True}
})
