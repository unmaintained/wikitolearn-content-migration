#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys ; sys.path.append('/opt/utils/')
from commons import *

sql_get_page_creation = "  SELECT "
sql_get_page_creation += " page_id "
sql_get_page_creation += " , "
sql_get_page_creation += " page_namespace "
sql_get_page_creation += " , "
sql_get_page_creation += " rev_id "
sql_get_page_creation += " , "
sql_get_page_creation += " page_title "
sql_get_page_creation += " , "
sql_get_page_creation += " timestamp "
sql_get_page_creation += " FROM wtl1_unified_revision "
sql_get_page_creation += " WHERE "
sql_get_page_creation += " rev_parent_id = 0"
sql_get_page_creation += " AND page_id IN (SELECT page_id FROM `wtl1_unified_revision` WHERE rev_parent_id = 0 GROUP BY page_id HAVING COUNT(*) > 1)"

cursor = source_db.cursor()
cursor.execute(sql_get_page_creation)
page_creation_list = mysql_fetchall(cursor)

mw_pages_collection = destination_db['mw_pages']

for page in tqdm(page_creation_list):
    search_for = {
        'page_id': page['page_id'],
        'timestamp': page['timestamp'],
    }
    mongo_object = mw_pages_collection.find_one(search_for)
    if mongo_object == None:
        mw_pages_collection.insert_one(page)
    else:
        mw_pages_collection.update_one(search_for,{'$set':page})
