#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys ; sys.path.append('/opt/utils/')
from commons import *

sql_get_page_creation = "  SELECT "
sql_get_page_creation += " page_id "
sql_get_page_creation += " , "
sql_get_page_creation += " page_namespace "
sql_get_page_creation += " , "
sql_get_page_creation += " rev_id "
sql_get_page_creation += " , "
sql_get_page_creation += " page_title "
sql_get_page_creation += " , "
sql_get_page_creation += " timestamp "
sql_get_page_creation += " , "
sql_get_page_creation += " NOW() AS timestamp_next "
sql_get_page_creation += " FROM wtl1_unified_revision "
sql_get_page_creation += " WHERE "
sql_get_page_creation += " rev_parent_id = 0"
sql_get_page_creation += " AND NOT page_id IN (SELECT page_id FROM `wtl1_unified_revision` WHERE rev_parent_id = 0 GROUP BY page_id HAVING COUNT(*) > 1)"

cursor = source_db.cursor()
cursor.execute(sql_get_page_creation)
page_creation_list = mysql_fetchall(cursor)

mw_pages_collection = destination_db['mw_pages']
for index_name in [
        "pages_id",
        "mw_page_id",
        "timestamp",
        "deleted",
        "category",
        "page_title",
        "timestamp_next",
        "rev_id",
        "page_namespace",
        "timestamp",
        "page_id",
        "page_title",
        "category",
        "all_used_titles",
        "superlog.page_title",
        "superlog.log_id",
        "superlog.rev_text_id",
        "superlog.page_id",
        "superlog.is_redirect",
        "superlog.rev_id",
        "superlog.sub_pages",
        "superlog.deleted",
        "superlog.params",
        "superlog.sub_pages_titles",
        "superlog.rev_parent_id",
        "superlog.page_namespace",
        "superlog.user_ids",
        "superlog.action",
        "superlog.type",
        "superlog.timestamp",
        "superlog.timestamp_next",
        "superlog.user_id"
    ]:
    mw_pages_collection.create_index([('index_name', pymongo.ASCENDING)])
    mw_pages_collection.create_index([('index_name', pymongo.DESCENDING)])


for page in tqdm(page_creation_list):
    search_for = {
        'page_id': page['page_id'],
        'timestamp': page['timestamp'],
    }
    mongo_object = mw_pages_collection.find_one(search_for)
    if mongo_object == None:
        mw_pages_collection.insert_one(page)
    else:
        mw_pages_collection.update_one(search_for,{'$set':page})
