#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys ; sys.path.append('/opt/utils/')
from commons import *

mw_pages_collection = destination_db['mw_pages']

pages_to_test = mw_pages_collection.find({'timestamp_next':{'$exists':False}})

if pages_to_test.count() > 0:
    for page in tqdm(pages_to_test, total=pages_to_test.count()):
        next_object = mw_pages_collection.find_one({
            'page_id': page['page_id'],
            'timestamp': {'$gt': page['timestamp']}
        }, sort=[("timestamp", 1)])

        if next_object == None:
            mw_pages_collection.update_one({'_id':page['_id']},{'$set':{
                'timestamp_next': datetime.datetime.now()
            }})
        else:
            mw_pages_collection.update_one({'_id':page['_id']},{'$set':{
                'timestamp_next': next_object['timestamp']
            }})
