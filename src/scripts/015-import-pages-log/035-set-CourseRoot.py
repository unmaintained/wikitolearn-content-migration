#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys ; sys.path.append('/opt/utils/')
from commons import *

mw_pages_collection = destination_db['mw_pages']

cursor = source_db.cursor()
cursor.execute("SELECT `cl_from` AS `page_id`, `cl_to` FROM `categorylinks` WHERE cl_to = 'CourseRoot'")
categorylinks = mysql_fetchall(cursor)

mw_pages_collection.update_many({}, {'$set':{'category':None}})

for categorylink in tqdm(categorylinks):
    mw_pages_collection.update_one({'page_id':categorylink['page_id']}, {'$set':{'category':categorylink['cl_to']}})
