#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys ; sys.path.append('/opt/utils/')
from commons import *

sql = ""
sql += "  SELECT "
sql += "  page_id "
sql += "  , "
sql += "  page_namespace "
sql += "  , "
sql += "  CAST(page_title AS binary) AS page_title "
sql += "  , "
sql += "  rev_id "
sql += "  , "
sql += "  rev_parent_id "
sql += "  , "
sql += "  rev_text_id "
sql += "  , "
sql += "  rev_user AS user_id "
sql += "  , "
sql += "  page_is_redirect AS is_redirect"
sql += "  , "
sql += "  STR_TO_DATE(rev_timestamp, '%Y%m%d%H%i%s') AS timestamp "
sql += "  FROM "
sql += "  page JOIN revision ON page.page_id = revision.rev_page "

sql += "  UNION "
sql += "  SELECT "
sql += "  ar_page_id AS page_id "
sql += "  , "
sql += "  ar_namespace AS page_namespace "
sql += "  , "
sql += "  CAST(ar_title AS binary) AS page_title "
sql += "  , "
sql += "  ar_rev_id AS rev_id "
sql += "  , "
sql += "  ar_parent_id AS rev_parent_id "
sql += "  , "
sql += "  ar_text_id AS rev_text_id "
sql += "  , "
sql += "  ar_user AS user_id "
sql += "  , "
sql += "  0 AS is_redirect"
sql += "  , "
sql += "  STR_TO_DATE(ar_timestamp, '%Y%m%d%H%i%s') AS timestamp "
sql += "  FROM "
sql += "  archive "

tab = "wtl1_unified_revision"

source_db.query("DROP TABLE IF EXISTS " + tab)
source_db.query("CREATE TABLE " + tab + " AS " + sql)
source_db.query("ALTER TABLE " + tab + " ADD INDEX(timestamp);")
source_db.query("ALTER TABLE " + tab + " ADD INDEX(page_id);")
