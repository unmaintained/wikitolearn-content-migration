#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys ; sys.path.append('/opt/utils/')
from commons import *

mw_pages_collection = destination_db['mw_pages']

all_pages = mw_pages_collection.find()
for page in tqdm(all_pages, total=all_pages.count()):
    for superlog_entry in page['superlog']:
        user_ids = []
        if 'user_ids' in superlog_entry:
            user_ids += superlog_entry['user_ids']
        if 'user_id' in superlog_entry:
            user_ids.append(superlog_entry['user_id'])
        user_ids = sorted(list(set(user_ids)))
        superlog_entry['all_users_ids'] = user_ids
    mw_pages_collection.update_one({'_id': page['_id']}, {'$set':{
        'superlog': page['superlog']
    }})
