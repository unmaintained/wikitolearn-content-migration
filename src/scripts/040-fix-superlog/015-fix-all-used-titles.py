#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys ; sys.path.append('/opt/utils/')
from commons import *

mw_pages_collection = destination_db['mw_pages']

all_pages = mw_pages_collection.find()
for page in tqdm(all_pages, total=all_pages.count()):
    all_used_titles = []
    for superlog_entry in page['superlog']:
        all_used_titles.append(superlog_entry['page_title'])
    all_used_titles = sorted(list(set(all_used_titles)))
    mw_pages_collection.update_one({'_id': page['_id']}, {'$set':{
        'all_used_titles': all_used_titles
    }})
