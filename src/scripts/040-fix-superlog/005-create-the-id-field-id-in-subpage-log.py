#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys ; sys.path.append('/opt/utils/')
from commons import *

mw_pages_collection = destination_db['mw_pages']

all_pages = mw_pages_collection.find()
for page in tqdm(all_pages, total=all_pages.count()):
    for superlog_entry in page['superlog']:
        if 'sub_pages' in superlog_entry:
            for sub_page in superlog_entry['sub_pages']:
                if len(sub_page['id_list']) == 0:
                    sub_page['id'] = None
                elif len(sub_page['id_list']) == 1:
                    sub_page['id'] = sub_page['id_list'][0]
                elif len(sub_page['id_list']) == 2:
                    page_0 = mw_pages_collection.find_one({
                        '_id': {
                            '$in': sub_page['id_list'],
                        }
                    }, sort=[('timestamp', pymongo.ASCENDING)])
                    page_1 = mw_pages_collection.find_one({
                        '_id': {
                            '$in': sub_page['id_list'],
                        }
                    }, sort=[('timestamp', pymongo.DESCENDING)])

                    if not page_0['superlog'][len(page_0['superlog'])-1]['deleted'] or \
                        page_1['superlog'][len(page_1['superlog'])-1]['deleted']:
                        raise ValueError("Deletion error")

                    last_timestamp = page_0['superlog'][0]['timestamp']
                    for superlog_entry in page_0['superlog']:
                        if last_timestamp > superlog_entry['timestamp']:
                            raise ValueError("Timestamp out of order")
                    for superlog_entry in page_1['superlog']:
                        if last_timestamp > superlog_entry['timestamp']:
                            raise ValueError("Timestamp out of order")

                    sub_page['id'] = page_1['_id']
                else:
                    raise ValueError("{}".format(sub_page['id_list']))
    mw_pages_collection.update_one({'_id': page['_id']}, {'$set':{
        'superlog': page['superlog']
    }})
