#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys ; sys.path.append('/opt/utils/')
from commons import *

mw_pages_collection = destination_db['mw_pages']

all_pages = mw_pages_collection.find({
    'superlog.sub_pages.id': {'$exists': True},
})

sub_level_oldest_timestamp = {}
sub_level_oldest_user_id = {}

for page in all_pages:
    for superlog_entry in page['superlog']:
        for sub_page in superlog_entry['sub_pages']:
            if not sub_page['id'] in sub_level_oldest_timestamp:
                sub_level_oldest_timestamp[sub_page['id']] = superlog_entry['timestamp']
                sub_level_oldest_user_id[sub_page['id']] = superlog_entry['user_id']
            elif sub_level_oldest_timestamp[sub_page['id']] > superlog_entry['timestamp']:
                sub_level_oldest_timestamp[sub_page['id']] = superlog_entry['timestamp']
                sub_level_oldest_user_id[sub_page['id']] = superlog_entry['user_id']

for sub_level_id in tqdm(sub_level_oldest_timestamp):
    if  sub_level_id != None:
        timestamp_lte = mw_pages_collection.find({
            '_id': sub_level_id,
            'superlog.timestamp': {
                '$lte': sub_level_oldest_timestamp[sub_level_id]
            }
        })
        if timestamp_lte.count() == 0:
            search_for_page_to_update = mw_pages_collection.find({
                '_id': sub_level_id,
            })
            if search_for_page_to_update.count() == 1:
                page_to_be_updated = list(search_for_page_to_update)[0]
                next_superlog = page_to_be_updated['superlog'][len(page_to_be_updated['superlog'])-1]
                creation =  {
                    "timestamp": sub_level_oldest_timestamp[sub_level_id],
                    "user_id": sub_level_oldest_user_id[sub_level_id],
                    "rev_text_id": None,
                    "params": None,
                    "page_namespace": next_superlog['page_namespace'],
                    "timestamp_next": next_superlog['timestamp'],
                    "page_wikitext": "",
                    "log_id": None,
                    "is_redirect": 0,
                    "rev_parent_id": None,
                    "page_html": "",
                    "rev_id": None,
                    "type": None,
                    "page_id": None,
                    "action": None,
                    "page_title": next_superlog['page_title'],
                    "deleted": False,
                }
                if page_to_be_updated['category'] in ["CourseRoot", "CourseLevelTwo"]:
                    creation['sub_pages'] = []
                    creation['sub_pages_titles'] = []
                page_to_be_updated['superlog'].insert(0, creation)
                mw_pages_collection.update_one({'_id':page_to_be_updated['_id']}, {
                    '$set':{'superlog': page_to_be_updated['superlog']}
                })
            else:
                sys.exit(1)
