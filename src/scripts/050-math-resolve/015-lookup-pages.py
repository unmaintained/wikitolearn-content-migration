#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys ; sys.path.append('/opt/utils/')
from commons import *
sys.exit(0)
pool_formulas = destination_aux_db['formulas']

formulas = pool_formulas.find({'$and':[
    {'formula_type': None},
    {'formula_type': {'$exists': True}},
]})

for formula_object in tqdm(formulas, total=formulas.count()):
    for lang in ['it', 'en', 'de', 'es', 'fr', 'pt', 'sv', 'ca']:
        mw_pages_collection = destination_link[lang]['mw_pages']
        pages = mw_pages_collection.find({
            'superlog.math_formulas': formula_object.get('formula')
        })
        for page in pages:
            if formula_object.get('formula') in page['superlog'][len(page['superlog'])-1]['math_formulas']:
                logger.error("{}: '{}' in {}".format(lang, formula_object.get('formula'), page['page_title']))
            else:
                logger.debug("{}: '{}' in {}".format(lang, formula_object.get('formula'), page['page_title']))
