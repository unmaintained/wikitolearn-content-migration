#!/usr/bin/env node

var MongoClient = require('mongodb').MongoClient;
var mongodb_url = "mongodb://destination:27017/";
var texvcjs = require('mathoid-texvcjs');

MongoClient.connect(mongodb_url, function(err, db) {
  if (err) throw err;
  var dbo = db.db("migration_shared_data");

  var query = {
    formula_texvcjs: {
      $exists: false
    }
  };

  dbo.collection("formulas").find(query).toArray(function(err, result) {
    if (err) throw err;
    result.forEach(function(item){
      // console.log("> " + item.formula);
      var texvcjs_result = texvcjs.check(item.formula);
      var update_filter = {'_id':item._id}
      if (texvcjs_result.status == '+') {
        var update_payload = {
          $set:{
            'formula_texvcjs':texvcjs_result.output
          }
        }
        dbo.collection("formulas").updateOne(update_filter, update_payload, function(err, res) {
          if (err) throw err;
          // console.log("Success " + item.formula);
        });
      } else {
        var update_payload = {
          $set:{
            'formula_texvcjs':null
          }
        }
        dbo.collection("formulas").updateOne(update_filter, update_payload, function(err, res) {
          if (err) throw err;
          console.error("Error: " + item.formula);
        });
      }
    });
    db.close();
  });


});
