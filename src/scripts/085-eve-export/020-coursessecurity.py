#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys ; sys.path.append('/opt/utils/')
from commons import *

wtl_revisions_collection = destination_db['wtl_revisions']

eve_base_url_coursesseciroty = os.environ.get("COURSESSECURITY_BACKEND_URI") + "/v1/"

page_ids = wtl_revisions_collection.find({
    'category': 'CourseRoot',
    'to_eve': 1,
}).distinct("mw_page_id")

for page_id in tqdm(page_ids):
    eve_id = None
    eve_etag = None
    eve_version = None
    current_private_state = None
    all_revisions = wtl_revisions_collection.find({
        'mw_page_id':page_id,
    }).sort("revision_number",pymongo.ASCENDING)

    for revision in tqdm(all_revisions, total=all_revisions.count()):
        if current_private_state != revision['private']:
            eve_revision_payload = {}
            eve_revision_payload['course'] = {
                '_id': revision['eve']['id'],
                '_version': revision['eve']['version'],
            }

            eve_revision_payload['private'] = revision['private']
            if revision['payload']['mw_userid_course_owner'] != None:
                eve_revision_payload['owner_id'] = str(revision['payload']['userid_course_owner'])
            else:
                eve_revision_payload['owner_id'] = str(-1)

            if eve_id == None or eve_etag == None:
                create_reply = requests.post(
                    url="{}coursessecurity".format(eve_base_url_coursesseciroty),
                    json=eve_revision_payload
                )
                create_reply.raise_for_status()
                create_obj = create_reply.json()
                eve_id = create_obj['_id']
                eve_etag = create_obj['_etag']
                eve_version = create_obj['_version']
            else:
                update_reply = requests.patch(
                    url="{}coursessecurity/{}".format(eve_base_url_coursesseciroty, eve_id),
                    json=eve_revision_payload,
                    headers={'If-Match': eve_etag}
                )
                update_reply.raise_for_status()
                update_obj = update_reply.json()
                eve_id = update_obj['_id']
                eve_etag = update_obj['_etag']
                eve_version = update_obj['_version']

            current_private_state = revision['private']
