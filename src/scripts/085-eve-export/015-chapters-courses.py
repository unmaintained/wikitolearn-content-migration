#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys ; sys.path.append('/opt/utils/')
from commons import *

wtl_revisions_collection = destination_db['wtl_revisions']

eve_base_url_courses = os.environ.get("COURSES_BACKEND_URI") + "/v1/"
eve_base_url_chapters = os.environ.get("CHAPTERS_BACKEND_URI") + "/v1/"

struct_order = [
    {
        'category': 'CourseLevelTwo',
        'eve_endpoint': eve_base_url_chapters + 'chapters',
        'eve_subpage_attribute': 'pages'
    },
    {
        'category': 'CourseRoot',
        'eve_endpoint': eve_base_url_courses + 'courses',
        'eve_subpage_attribute': 'chapters',
    },
]

for struct_item_to_do in struct_order:
    print("\n\n")
    print(struct_item_to_do)
    print("\n\n")

    page_ids = wtl_revisions_collection.find({
        'category': struct_item_to_do['category'],
        'to_eve': 1,
    }).distinct("mw_page_id")

    for page_id in tqdm(page_ids):
        eve_id = None
        eve_etag = None
        eve_version = None
        all_revisions = wtl_revisions_collection.find({
            'mw_page_id':page_id,
        }).sort("revision_number",pymongo.ASCENDING)

        for revision in tqdm(all_revisions, total=all_revisions.count()):
            if 'eve' in revision:
                eve_id = revision['eve']['id']
                eve_etag = revision['eve']['etag']
                eve_version = revision['eve']['version']
            else:
                eve_revision_payload = {}
                eve_revision_payload['title'] = revision['page_title']
                eve_sub_pages_list = []
                for sub_page in revision['payload']['subpages']:
                    if sub_page['id'] != None:
                        eve_ref_subpage = wtl_revisions_collection.find_one({
                            'mw_page_id': sub_page['id'],
                            'revision_number': sub_page['rev'],
                        })
                        eve_sub_pages_list.append({
                            '_id': eve_ref_subpage['eve']['id'],
                            '_version': eve_ref_subpage['eve']['version'],
                        })
                eve_revision_payload[struct_item_to_do['eve_subpage_attribute']] = eve_sub_pages_list
                eve_revision_payload['authors'] = revision['user_ids']
                eve_revision_payload['language'] = lang
                if eve_id == None or eve_etag == None:
                    create_reply = requests.post(
                        url="{}".format(struct_item_to_do['eve_endpoint']),
                        json=eve_revision_payload
                    )
                    create_reply.raise_for_status()
                    create_obj = create_reply.json()
                    eve_id = create_obj['_id']
                    eve_etag = create_obj['_etag']
                    eve_version = create_obj['_version']
                else:
                    update_reply = requests.patch(
                        url="{}/{}".format(struct_item_to_do['eve_endpoint'], eve_id),
                        json=eve_revision_payload,
                        headers={'If-Match': eve_etag}
                    )
                    update_reply.raise_for_status()
                    update_obj = update_reply.json()
                    eve_id = update_obj['_id']
                    eve_etag = update_obj['_etag']
                    eve_version = update_obj['_version']
                wtl_revisions_collection.update_one({
                    '_id':revision['_id'],
                }, {'$set' :{
                    'eve': {
                        'id': eve_id,
                        'etag': eve_etag,
                        'version': eve_version,
                    },
                }})
