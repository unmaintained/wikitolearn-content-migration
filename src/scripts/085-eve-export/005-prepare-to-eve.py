#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys ; sys.path.append('/opt/utils/')
from commons import *

wtl_revisions_collection = destination_db['wtl_revisions']

wtl_revisions_collection.update_many({}, {'$set': {'to_eve':1}})

sys.exit(0) # HACK

wtl_revisions_collection.update_many({}, {'$unset': {'eve':1}})

wtl_revisions_collection.update_many({}, {'$unset': {'to_eve':1}})

page_titles_to_export = []

if lang == "it":
    page_titles_to_export = [
        "Rete IP",
        "Prova", # Ale/Prova
        "Elettromagnetismo", # Dan/Elettromagnetismo
        "Meccanica Analitica",
        "Fondamenti di informatica",
    ]
elif lang == "en":
    page_titles_to_export = [
        "Linear Algebra",
        "Multivariable Calculus",
        "Data Structures",
    ]

wtl_revisions_collection.update_many({
    'category': 'CourseRoot',
    'page_title': {'$in': page_titles_to_export},
}, {'$set':{
    'to_eve': 1,
}})

page_lv2_ids = wtl_revisions_collection.find({
    'category': 'CourseRoot',
    'to_eve': 1,
}).distinct("payload.subpages.id")
for lv2_id in page_lv2_ids:
    wtl_revisions_collection.update_many({
        'mw_page_id': lv2_id,
    }, {'$set':{
        'to_eve': 1,
    }})

page_lv3_ids = wtl_revisions_collection.find({
    'category': 'CourseLevelTwo',
    'to_eve': 1,
}).distinct("payload.subpages.id")
for lv3_id in page_lv3_ids:
    wtl_revisions_collection.update_many({
        'mw_page_id': lv3_id,
    }, {'$set':{
        'to_eve': 1,
    }})
