#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys ; sys.path.append('/opt/utils/')
from commons import *

all_cache_objects = destination_aux_db['parsoid_cache'].find({'$or':[
    {'tested_hostname': {'$ne': True}},
]})

for cache_entry in tqdm(all_cache_objects, total=all_cache_objects.count()):
    bs_object = BeautifulSoup(cache_entry['parsoid_text'], 'lxml')
    base_tags = bs_object.find_all('base')
    if len(base_tags) == 0:
        pass
    elif len(base_tags) == 1:
        o = urlparse(str(base_tags[0].attrs['href']))
        hostname = o.netloc
        if hostname == "it.wikitolearn.org" or \
            hostname == "en.wikitolearn.org" or \
            hostname == "de.wikitolearn.org" or \
            hostname == "es.wikitolearn.org" or \
            hostname == "fr.wikitolearn.org" or \
            hostname == "pt.wikitolearn.org" or \
            hostname == "ca.wikitolearn.org":
            destination_aux_db['parsoid_cache'].update_one({
                '_id':cache_entry['_id'],
            },{'$set': {
                'tested_hostname': True,
            }})
        else:
            domain = hostname[hostname.find('.'):]
            parsoid_text = cache_entry['parsoid_text'].replace(domain, ".wikitolearn.org")

            bs_object = BeautifulSoup(parsoid_text, 'lxml')
            destination_aux_db['parsoid_cache'].update_one({
                '_id':cache_entry['_id'],
            },{'$set': {
                'tested_hostname': True,
                'parsoid_text': parsoid_text
            }})
    else:
        raise ValueError()
