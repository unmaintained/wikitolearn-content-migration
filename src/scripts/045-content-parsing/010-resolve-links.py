#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys ; sys.path.append('/opt/utils/')
from commons import *

pool_files = destination_link['shared']['files']
db_source_pool =   MySQLdb.connect('source', 'root', 'root', "pool"+"wikitolearn", use_unicode=True, charset="utf8mb4")

mw_pages_collection = destination_db['mw_pages']

def lookup_file(page, filename):
    try:
        filename = filename.replace(' ', '_')

        filedb_entry = pool_files.find_one({'filename':filename})
        if filedb_entry == None:
            pool_files.insert_one(
                {
                    'filename':filename,
                    'page_id': [page['_id']]
                }
            )
            filedb_entry = pool_files.find_one({'filename':filename})
        else:
            list_pages_id = []
            if 'page_id' in filedb_entry:
                list_pages_id = filedb_entry['page_id']
            list_pages_id += [page['_id']]
            pool_files.update_one(
                {'_id':filedb_entry['_id']},
                {'$set':{
                    'page_id': list_pages_id
                }}
            )

        # search in the lang db
        sql = "SELECT * FROM `page` WHERE CAST(page_title AS binary) LIKE %s"

        cursor = source_db.cursor()
        cursor.execute(sql, (filename, ))
        search_in_mysql = mysql_fetchall(cursor)
        if len(search_in_mysql) > 0:
            # fonud in the lang db
            pool_files.update_one(
                {'_id':filedb_entry['_id']},
                {'$set':{
                    'source': lang
                }}
            )
        else:
            # search in pool
            sql = "SELECT * FROM `page` WHERE CAST(page_title AS binary) LIKE %s"

            cursor = db_source_pool.cursor()
            cursor.execute(sql, (filename, ))
            search_in_mysql_pool = mysql_fetchall(cursor)
            if len(search_in_mysql_pool) > 0:
                # found in pool
                pool_files.update_one(
                    {'_id':filedb_entry['_id']},
                    {'$set':{
                        'source': 'pool'
                    }}
                )
            else:
                # is commons
                pool_files.update_one(
                    {'_id':filedb_entry['_id']},
                    {'$set':{
                        'source': 'commons'
                    }}
                )
    except Exception as e:
        print(e)
        sys.exit(1)

def lookup_page(page, link):
    if '#' in link:
        link = link[0:link.find('#')]
    page_to_link = mw_pages_collection.find({
        'superlog.page_title': link.strip(),
        'page_namespace': {'$ne': 2900},
    })
    if page_to_link.count() != 1:
        raise ValueError("Page '{}'".format(link))

all_pages = mw_pages_collection.find({
    'category': 'CourseLevelThree',
})

pages_to_fix = {}
for page in tqdm(all_pages, total=all_pages.count()):
    if not page['superlog'][-1]['deleted']:
        for link in page['superlog'][-1]['links']: # HACK
            try:
                if link[0:len("./Categoria:")] == "./Categoria:" or \
                    link[0:len("./Kategorie:")] == "./Kategorie:" or \
                    link[0:len("./Category:")] == "./Category:":
                    pass
                    # raise ValueError(link)

                elif  link[0:len("./File:")] == "./File:":
                    lookup_file(page, unquote(link[len("./File:"):]))
                elif  link[0:len("./Archivo:")] == "./Archivo:":
                    lookup_file(page, unquote(link[len("./Archivo:"):]))
                elif  link[0:len("https://" + lang + ".wikitolearn.org/File:")] == "https://" + lang + ".wikitolearn.org/File:":
                    lookup_file(page, unquote(link[len("https://" + lang + ".wikitolearn.org/File:"):]))

                elif  link[0:len("./Corso:")] == "./Corso:":
                    lookup_page(page, link[len("./Corso:"):])
                elif  link[0:len("./Course:")] == "./Course:":
                    lookup_page(page, link[len("./Course:"):])

                elif  link[0:len("./Utente:")] == "./Utente:":
                    lookup_page(page, link[len("./Utente:"):])
                elif  link[0:len("./User:")] == "./User:":
                    lookup_page(page, link[len("./User:"):])

                elif link[0:len("./Main_Page#cite_ref")] == "./Main_Page#cite_ref":
                    pass # citation
                elif link[0:len("./Main_Page#cite_note")] == "./Main_Page#cite_note":
                    pass # citation
                elif link[0:len("//" + lang +".wikitolearn.org/images/")] == "//" + lang +".wikitolearn.org/images/":
                    pass
                elif link[0:len("//pool.wikitolearn.org/images/")] == "//pool.wikitolearn.org/images/":
                    pass
                elif link[0:len("//restbase.wikitolearn.org/")] == "//restbase.wikitolearn.org/":
                    pass
                elif link[0:len("//upload.wikimedia.org/wikipedia/commons/")] == "//upload.wikimedia.org/wikipedia/commons/":
                    pass
                elif  link[0:len("http://")] == "http://":
                    pass
                elif  link[0:len("https://")] == "https://":
                    pass
                elif  link[0:len("//")] == "//":
                    pass
                elif  link[0:len("mailto:")] == "mailto:":
                    pass
                elif  link[0:len("./")] == "./":
                    raise ValueError("> {}".format(link))
                # else:
                #     raise ValueError()
            except Exception as e:
                pageurl = "https://{}.wikitolearn.org/".format(lang)
                if page['page_namespace'] == 2:
                    pageurl += "User:"
                else:
                    pageurl += "Course:"

                pageurl += requests.utils.quote(page['page_title'])

                pageurl = pageurl.strip()
                link = link.strip()

                if pageurl not in pages_to_fix:
                    pages_to_fix[pageurl] = []
                pages_to_fix[pageurl].append(link)
                logger.warn(pageurl + " : " + requests.utils.quote(link))

if len(pages_to_fix) > 0:
    if not os.path.isdir("/srv/pages_to_fix/"):
        os.mkdir("/srv/pages_to_fix/")
    with open("/srv/pages_to_fix/links-" + lang + ".yml", "w") as fh:
        yaml.dump(pages_to_fix, fh,  default_flow_style=False)
