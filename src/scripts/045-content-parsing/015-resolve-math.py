#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys ; sys.path.append('/opt/utils/')
from commons import *

pool_formulas = destination_aux_db['formulas']
db_source_pool =   MySQLdb.connect('source', 'root', 'root', "pool"+"wikitolearn", use_unicode=True, charset="utf8mb4")

mw_pages_collection = destination_db['mw_pages']

all_pages = mw_pages_collection.find({
    'category': 'CourseLevelThree',
})


for page in tqdm(all_pages, total=all_pages.count()):
    for formula in page['superlog'][len(page['superlog'])-1]['math_formulas']: # HACK
        formula_hash = hashlib.sha256(formula.encode('utf-8')).hexdigest()

        if pool_formulas.find_one({'formula_hash':formula_hash}) == None:
            pool_formulas.insert_one({
                'formula': formula
            })
