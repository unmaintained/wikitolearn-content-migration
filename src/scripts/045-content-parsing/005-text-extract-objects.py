#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys ; sys.path.append('/opt/utils/')
from commons import *

mw_pages_collection = destination_db['mw_pages']

all_pages = mw_pages_collection.find({
    'category': 'CourseLevelThree',
})

def worker_fn(page):
    for superlog_entry in page['superlog']:
        links = []
        math_formulas = []
        templates = []
        soup = BeautifulSoup(superlog_entry['page_html'], "lxml")

        for html_template_object in soup.findAll(attrs={'typeof': 'mw:Transclusion'}):
            json_template_object = json.loads(html_template_object['data-mw'])
            for part in json_template_object['parts']:
                if type(part) != str:
                    templates.append(part['template']['target']['wt'])

        for html_math_object in soup.findAll(attrs={'typeof': 'mw:Extension/math'}):
            json_math_object = json.loads(html_math_object['data-mw'])
            formula = json_math_object.get('body').get('extsrc')
            math_formulas.append(formula)

        for link in soup.findAll(attrs={'href':True}):
            (link,) = link.attrs.get('href'),
            if not link in links:
                links.append(link)
        for link in soup.findAll(attrs={'src':True}):
            (link,) = link.attrs.get('src'),
            if not link in links:
                links.append(link)

        superlog_entry['links'] = list(set(links))
        superlog_entry['math_formulas'] = list(set(math_formulas))
        superlog_entry['templates'] = list(set(templates))
    return (page['_id'], page)

multiprocessing_pool = multiprocessing.Pool(multiprocessing.cpu_count())

for page_id_to_link in tqdm(multiprocessing_pool.imap(worker_fn, all_pages), total=all_pages.count()):
    (page_id, page) = page_id_to_link
    mw_pages_collection.update_one({
        '_id': page_id,
    }, {
        '$set': page,
    })
