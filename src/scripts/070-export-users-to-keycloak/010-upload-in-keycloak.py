#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys ; sys.path.append('/opt/utils/')
from commons import *

mw_users_collection = destination_db['users']

all_users = mw_users_collection.find()
for user in tqdm(all_users,total=all_users.count()):
    if 'keycloak_user' in user:
        user_search_by_username_endpoint = keycloak_realm_base_url + "/users?username={}".format(user['keycloak_user']['username'])
        search_r = requests.get(user_search_by_username_endpoint,headers=keycloak_get_auth_headers(),verify=keycloak_requests_verify)
        search_data = search_r.json()
        found = False
        for search_item in search_data:
            if search_item['username'] == user['keycloak_user']['username']:
                found = True
        if not found:
            users_endpoint = keycloak_realm_base_url + "/users"
            create_user_request = requests.post(
                users_endpoint,
                json=user['keycloak_user'],
                headers=keycloak_get_auth_headers(),
                verify=keycloak_requests_verify,
            )
            create_user_request.raise_for_status()
        else:
            pprint(search_data)
            pprint(user)
