#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys ; sys.path.append('/opt/utils/')
from commons import *

mw_users_collection = destination_db['users']

keep_running = True
running_iteration_count = 0
while keep_running:
    users_url = keycloak_realm_base_url + "/users?first={}&max=500".format(500*running_iteration_count)
    all_keycloak_users = requests.get(users_url,headers=keycloak_get_auth_headers(),verify=keycloak_requests_verify)
    for keycloak_uesr in tqdm(all_keycloak_users.json()):
        mongodb_search_for_uesr = mw_users_collection.find({
            'mw_user_id':int(keycloak_uesr.get('attributes').get('mw_id')[0]),
        })
        if mongodb_search_for_uesr.count() != 1:
            raise ValueError("Multiple match for the user {}".format(keycloak_uesr.get('id')))
        for mongodb_user_object in mongodb_search_for_uesr:
            mw_users_collection.update_one({
                '_id': mongodb_user_object['_id']
            }, {'$set': {
                    'keycloak_id': keycloak_uesr.get('id')
                },
            })
    keep_running = len(all_keycloak_users.json()) > 0
    running_iteration_count += 1
