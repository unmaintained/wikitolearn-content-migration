#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# HACK this script is to obfuscate data about users in keycloak
import sys ; sys.path.append('/opt/utils/')
from commons import *

keep_running = True
running_iteration_count = 0
while keep_running:
    users_url = keycloak_realm_base_url + "/users?first={}&max=500".format(500*running_iteration_count)
    all_keycloak_users = requests.get(users_url,headers=keycloak_get_auth_headers(),verify=keycloak_requests_verify)
    for keycloak_uesr in tqdm(all_keycloak_users.json()):
        for attkey in list(set(keycloak_uesr['attributes'].keys()) - set(["mw_id"])):
            del keycloak_uesr['attributes'][attkey]
        keycloak_uesr['email'] = '{}@localhost.lan'.format(keycloak_uesr.get('username'))
        requests.put(
            keycloak_realm_base_url + "/users/{}".format(keycloak_uesr.get('id')),
            json=keycloak_uesr,
            headers=keycloak_get_auth_headers(),
            verify=keycloak_requests_verify
        ).raise_for_status()
        requests.put(
            keycloak_realm_base_url + "/users/{}/reset-password".format(keycloak_uesr.get('id')),
            json={
                "type":"password",
                "value":"DummyPassword00000000",
                "temporary":False
            },
            headers=keycloak_get_auth_headers(),
            verify=keycloak_requests_verify
        ).raise_for_status()
    keep_running = len(all_keycloak_users.json()) > 0
    running_iteration_count += 1
