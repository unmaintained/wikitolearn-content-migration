#!/bin/bash
if [[ "$1" != "" ]]
then
  export DIR_FILTER=$1
  shift
else
  export DIR_FILTER=""
fi

if [[ "$1" != "" ]]
then
  export FILE_FILTER=$1
  shift
else
  export FILE_FILTER=""
fi


BASE_PATH=/opt/scripts/
cd $BASE_PATH

function run_script() {
  file="$1"
  export PYTHONUNBUFFERED=1
  export PYTHONOPTIMIZE=1
  echo -n -e '\e[0;34m'
  echo ">> File: "${file##$BASE_PATH}" (LANG="$WTL_LANG")"
  echo -n -e '\e[0m'
  echo
  time $file
  ES=$?
  if [[ $ES -ne 0 ]]
  then
    echo -n -e '\e[0;31m'
    echo
    echo "Exist status "$ES
    echo
    echo -n -e '\e[0m'
    exit 1
  fi
  echo
  echo
}

for sub in $BASE_PATH$DIR_FILTER*
do
  echo -n -e '\e[1;32m'
  echo "> Directory: "${sub##$BASE_PATH}
  echo -n -e '\e[0m'
  if test -f $sub/.skipme
  then
    echo "skipping..."
    echo
  else
    cd $sub
    for file in $sub/$FILE_FILTER*
    do
      if test -f $sub/.shared
      then
        export WTL_LANG=shared
        run_script $file
      else
        for WTL_LANG in it en de es fr pt sv ca # FIXME
        do
          export WTL_LANG
          run_script $file
        done
      fi
    done
  fi
done
